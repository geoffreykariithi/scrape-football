package com.tunes_developers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tunes_developers.Models.Link;
import com.tunes_developers.Models.StagePlayer;
import com.tunes_developers.Models.Team;
import io.webfolder.ui4j.api.browser.*;
import io.webfolder.ui4j.api.interceptor.Interceptor;
import io.webfolder.ui4j.api.interceptor.Request;
import io.webfolder.ui4j.api.interceptor.Response;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.IOException;
import java.net.ConnectException;
import java.util.ArrayList;
import java.util.List;

import static io.webfolder.ui4j.api.browser.BrowserFactory.getWebKit;

public class Main {

    public static void main(String[] args) throws IOException, InterruptedException {
//	    String base = "https://www.whoscored.com";
//	    BrowserEngine webkit = BrowserFactory.getWebKit();
//        Interceptor interceptor = new Interceptor() {
//            @Override
//            public void beforeLoad(Request request) {
//
//            }
//
//            @Override
//            public void afterLoad(Response response) {
//                System.out.println("Response \n"+response.toString());
//            }
//        };
//        PageConfiguration configuration = new PageConfiguration(interceptor);
//        configuration.setSelectorEngine(SelectorType.SIZZLE);

//	    ObservableList<Link> links = Extractor.extractLinks(webkit, base);
//	    links = Extractor.extractTeamInfo(webkit, links);
        ObservableList<Link> skipped = FXCollections.observableArrayList();

//        skipped.add(new Link("France","France","https://www.whoscored.com/Regions/74/Tournaments/22/France-Ligue-1"));
//        skipped.add(new Link("Portugal","Portugal","https://www.whoscored.com/Regions/177/Tournaments/21/Portugal-Liga-NOS"));
//        skipped.add(new Link("Netherlands","Netherlands","https://www.whoscored.com/Regions/155/Tournaments/13/Netherlands-Eredivisie"));
//        skipped.add(new Link("Turkey","Turkey","https://www.whoscored.com/Regions/225/Tournaments/17/Turkey-Super-Lig"));
//        skipped = Extractor.extractTeamInfo(webkit, skipped);

        Gson gson = new Gson();
        String json = File.getData("G:\\Workspace\\web\\playground\\football\\links.json");
        List<Link> tasks = gson.fromJson(json, new TypeToken<List<Link>>(){}.getType());
        ObservableList<Link> detailedLinks =  FXCollections.observableArrayList(tasks);
        ObservableList<Link> emptyLinks = FXCollections.observableArrayList();
        ObservableList<Link> missingLinks = FXCollections.observableArrayList();

//        for (Link l : detailedLinks) {
//            System.out.println(l.getTeams().size());
//
//            if(l.getTeams().size() > 0 && l.getTeams().get(0).getStages() != null && l.getTeams().get(0).getStages().size() > 0){
//                missingLinks.add(l);
//            }else {
//                emptyLinks.add(l);
//            }
//        }
//
//        Extractor.findTeamStages(webkit, emptyLinks, missingLinks);



//            (new WebDriverWait(driver, 10)).until(new ExpectedCondition<Boolean>() {
//                public Boolean apply(WebDriver d) {
//                    System.out.println(d.findElements(By.cssSelector("#player-table-statistics-body tr")).size());
//                    return d.findElements(By.cssSelector("#player-table-statistics-body tr")).size() > 0;
//                }
//            });
        String exePath = "G:\\football\\chromedriver.exe";
        System.setProperty("webdriver.chrome.driver", exePath);


        WebDriver driver = new ChromeDriver();
//        driver.get("https://www.whoscored.com/Teams/167/Archive/England-Manchester-City?stageId=13796");
//        System.out.println(driver.getTitle()+" "+driver.getCurrentUrl());
        Extractor.extractPlayers(driver,detailedLinks);

//        Extractor.findPlayers(webkit,detailedLinks);
//        ObservableList<StagePlayer> stagePlayers = FXCollections.observableArrayList();
//        StagePlayer player = new StagePlayer(
//                true,
//                "James",
//                "Gor",
//                315,
//                1234,
//                24,
//                "CF",
//                123,
//                23,
//                "12",
//                901,
//                2,
//                3,
//                2,
//                1,
//                0.8,
//                92.4,
//                1.3,
//                3,
//                8.34
//        );
//
//        stagePlayers.add(player);
//        Extractor.writeTeamPlayer(stagePlayers);
//        try {
//            Extractor.findTeamStages(webkit, findEmptyStages(detailedLinks));
//        }catch (ConnectException ex){
//            Extractor.findTeamStages(webkit, findEmptyStages(detailedLinks));
//        }
//        Extractor.extractStages(webkit,detailedLinks);
//        engineModel = (EngineModel)gson.fromJson(file.getData(), EngineModel.class);
//        webkit.shutdown();
//        BrowserEngine webkit = BrowserFactory.getWebKit();
//        PageConfiguration configuration = new PageConfiguration();
//        configuration.setSelectorEngine(SelectorType.SIZZLE);
//        try (Page page = webkit.navigate("https://www.whoscored.com/Regions/252/Tournaments/2/England-Premier-League")) {
//			page.getDocument()
//					.queryAll("tbody.standings a.team-link")
//					.forEach(e -> {
//						System.out.println(e.getText().get()+" "+e.getAttribute("href").get());
//					});
//
//            page.close();
//            webkit.shutdown();
//		}
    }
}
