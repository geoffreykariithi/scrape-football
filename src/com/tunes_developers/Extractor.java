package com.tunes_developers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tunes_developers.Models.*;
import io.webfolder.ui4j.api.browser.*;
import io.webfolder.ui4j.api.dom.Element;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class Extractor {
    static String location = "G:\\Workspace\\web\\playground\\football\\links.json";
    static String path = "G:\\football\\teams\\";
    static String logs = "G:\\football\\logs.txt";
    static int counter = 0;

    public static ObservableList<Link> extractLinks(BrowserEngine webkit, String base) throws IOException, InterruptedException {
        ObservableList<Link> links = FXCollections.observableArrayList();

        Page page = webkit.navigate(base);
            page.getDocument()
                    .queryAll("ul#popular-tournaments-list li a.pt")
                    .forEach(e -> {
                        String link = base+e.getAttribute("href").get();
                        String title = e.getAttribute("title").get();
                        String html = e.getInnerHTML();
                        String name = html.substring(0,html.indexOf("<span"));

                        links.add(new Link(name,title,link));
                    });

//            page.close();
//        }

        //Save to file
        writeToFile(links);
        return links;
    }

    public static void writeToFile(ObservableList<Link> links) throws IOException {
        Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
        String data = gson.toJson(links);
        File.writeText(location,data);
        System.out.println("Completed Writting Links\n Total Number Written: "+links.size());
    }

    public static void writeTeamPlayer(ObservableList<StagePlayer> players) throws IOException {
        if(players.size() > 0){
            Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
            String data = gson.toJson(players);
            StagePlayer p = players.get(0);
            File.writeText(path+p.getTeam()+"-"+p.getStageId()+".json",data);
            System.out.println("Completed Writting Links\n Total Number Written: "+players.size());
//            Extractor.writeLog("SUCCESS :: Successfully Wrote player data for the team: "+ players.get(0).getTeam());
        }
    }

    public static void writeLog(String txt) throws IOException {
        String data = File.getData(logs);
        Date d = new Date();
        File.writeText(logs,data+"\n"+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds()+"   "+txt+"\n");
    }

    public static ObservableList<Link> extractTeamInfo(BrowserEngine webkit, ObservableList<Link> links) throws IOException, InterruptedException {
        for (Link link : links) {
            ObservableList<Team> teams = getTeams(webkit,link.link);
            link.setTeams(new ArrayList<>(teams.stream().collect(Collectors.toList())));
        }

        //Save to file
        writeToFile(links);
        return  links;
    }

    public static ObservableList<Link> findTeamStages(BrowserEngine webkit, ObservableList<Link> links, ObservableList<Link> missingLinks) throws IOException, InterruptedException {
        for (Link link : links){
            for (Team t : link.getTeams()){
//                ObservableList<Stage> stages = ExtractorJsoup.extractStages(t.getLink().replace("Show","Archive"));
//                t.setStages(stages);

                t.setStages(new ArrayList<>(ExtractorJsoup.extractStages(webkit,t.getLink().replace("Show","Archive"),0).stream().collect(Collectors.toList())));
            }
            missingLinks.add(link);
            writeToFile(missingLinks);
        }

        return links;
    }

    public static ObservableList<Stage> extractStages(BrowserEngine webkit, String link,int counter) throws IOException, InterruptedException {
        ObservableList<Stage> stages = FXCollections.observableArrayList();

        Page page = webkit.navigate(link);
        page.getDocument()
                .queryAll("#stageId option")
                .forEach(e -> {
                    int id = Integer.parseInt(e.getAttribute("value").get());
                    String name = e.getInnerHTML();

                    stages.add(new Stage(id,name));
                });

        if(stages.size() > 0){
            System.out.println("Operation Successfull");
            return stages;
        }else{
            counter++;

            //If there are no stages found fall back wait for seven seconds then execute to method number 1 of extraction
            if(counter <= 5){
                Thread.sleep(36000);
                System.out.println("Reverting to method 2");
                return ExtractorJsoup.extractStages(webkit,link,counter);
            }else{
                return stages;
            }
        }
    }


    public static void findPlayers(BrowserEngine webkit, ObservableList<Link> links) throws IOException, InterruptedException {
        for (Link link : links){
            for (Team t : link.getTeams()){
               ObservableList<StagePlayer> players = FXCollections.observableArrayList();

               for (Stage s : t.getStages()){
                   players = combineList(players,extractPlayerInformation(webkit,t,t.getLink()+"?stageId="+s.getId(),counter));
               }

                writeTeamPlayer(players);
            }

        }
    }

    public static void extractPlayers(WebDriver driver, ObservableList<Link> links) throws IOException, InterruptedException {
        for (Link link : links){
            for (Team t : link.getTeams()){
                ObservableList<StagePlayer> players = FXCollections.observableArrayList();

                for (Stage s : t.getStages()){
                    String playerLink = t.getLink()+"?stageId="+s.getId();
                    playerLink = playerLink.replace("Show","Archive");
//                    players = combineList(players,getTeamInfo(driver,playerLink,t));
                    try{
                        players = getTeamInfo(driver,playerLink,t);
                    }catch (Exception e){
                        players = getTeamInfo(driver,playerLink,t);
                    }

                    writeTeamPlayer(players);
                }

            }
        }
    }

    public static boolean hasClass(WebElement element,String className) {
        String classes = element.getAttribute("class");

        if(classes.contains(className)){
            return true;
        }

        return false;
    }

    public static ObservableList<StagePlayer> getTeamInfo(WebDriver driver, String link, Team team) throws InterruptedException {
        ObservableList<StagePlayer> stagePlayers = FXCollections.observableArrayList();
        ObservableList<StagePlayer> detailedPlayers = FXCollections.observableArrayList();
        List<WebElement> tabs = new ArrayList<>();
        int stageId = getNumber(link.substring(link.indexOf("=")+1));

        //Wait for data to load
        driver.get(link);
        WebDriverWait wait = new WebDriverWait(driver,360);
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.cssSelector("#statistics-table-summary #player-table-statistics-body tr"),5));

        WebElement body = driver.findElement(By.cssSelector("body"));
        //Extract Tabs
        tabs = body.findElements(By.cssSelector("ul.tabs li.in-squad-detailed-view a"));
        System.out.println(tabs.size());

        List<WebElement> players = driver.findElements(By.cssSelector("#statistics-table-summary #player-table-statistics-body tr"));
        for (WebElement e : players){
            List<WebElement> elements = e.findElements(By.cssSelector("td"));

            WebElement el = elements.get(2).findElement(By.cssSelector(".player-link"));
            boolean isAvailable = true;
            if(hasClass(e,"not-current-player")){
                isAvailable = false;
            }
            //Player's Name
            String name = el.getText();
            String playerLink = el.getAttribute("href");

            //Player ID
            int playerId = getNumber(playerLink.substring(playerLink.indexOf("Players/")+8,playerLink.indexOf("/Show")));

            //Player shirtNumber and position
            List<WebElement> playerDetails = elements.get(2).findElements(By.cssSelector(".player-meta-data"));
            int shirtNumber = getNumber(playerDetails.get(1).getText());
            String playerPositions = playerDetails.get(2).getText();

            //Player body details
            int height = getNumber(elements.get(3).getText());
            int weight = getNumber(elements.get(4).getText());

            //Player Match stats
            String appearance = elements.get(5).getText();
            int minutesPlayed = getNumber(elements.get(6).getText());
            int goals = getNumber(elements.get(7).getText());
            int assists = getNumber(elements.get(8).getText());
            int yellowCards = getNumber(elements.get(9).getText());
            int redCards = getNumber(elements.get(10).getText());
            String shotsPerGame = getDouble(elements.get(11).getText());
            String passSuccess = getDouble(elements.get(12).getText());
            String aerialsWon = getDouble(elements.get(13).getText());
            int manOfTheMatch = getNumber(elements.get(14).getText());
            String rating = getDouble(elements.get(15).getText());

            StagePlayer player = new StagePlayer(
                    stageId,
                    isAvailable,
                    name,
                    team.getName(),
                    team.getTeamId(),
                    playerId,
                    shirtNumber,
                    playerPositions,
                    height,
                    weight,
                    appearance,
                    minutesPlayed,
                    goals,
                    assists,
                    yellowCards,
                    redCards,
                    shotsPerGame,
                    passSuccess,
                    aerialsWon,
                    manOfTheMatch,
                    rating
            );

            stagePlayers.add(player);
            System.out.println(player.toString());
        }

        System.out.println("Players Found: "+stagePlayers.size());

        String tacklesPerGame = "0";
        String interceptionsPerGame = "0";
        String foulsPerGame = "0";
        String offsidesPerGame = "0";
        String clearancePerGame = "0";
        String wasDribbledPerGame = "0";
        String blocksPerGame = "0";
        int ownGoal = 0;
        String keyPassesPerGame = "0";
        String dribblesWonPerGame = "0";
        String fouledPerGame = "0";
        String dispossessedPerGame = "0";
        String turnoverPerGame = "0";
        String totalPassesPerGame = "0";
        String accurateCrossesPerGame = "0";
        String accurateLongPassesPerGame = "0";
        String accurateThroughBallPerGame = "0";
        String shotsOBox = "0";
        String shotsSixYardBox = "0";
        String shotsPenaltyArea = "0";

            // Extract Tab Defensive
            StagePlayer p = new StagePlayer(10000001);

        for (int i = 0; i < tabs.size(); i++) {
            WebElement tab = tabs.get(i);

            System.out.println(tab.getText());
            if(tab.getText().equals("Defensive")){
                JavascriptExecutor executor = (JavascriptExecutor)driver;
                executor.executeScript("arguments[0].click();", tab);
                wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.cssSelector("#statistics-table-defensive #player-table-statistics-body tr"),5));

                List<WebElement> rows = body.findElements(By.cssSelector("#statistics-table-defensive #player-table-statistics-body tr"));
                for (WebElement element: rows){

                    List<WebElement> elements = element.findElements(By.tagName("td"));

                    // Get Player ID
                    WebElement el = elements.get(2).findElement(By.cssSelector(".player-link"));
                    String playerLink = el.getAttribute("href");
                    int playerId = getNumber(playerLink.substring(playerLink.indexOf("Players/")+8,playerLink.indexOf("/Show")));

                    p = findPlayer(stagePlayers,playerId);

                    tacklesPerGame = getDouble(elements.get(7).getText());
                    interceptionsPerGame = getDouble(elements.get(8).getText());
                    foulsPerGame = getDouble(elements.get(9).getText());
                    offsidesPerGame = getDouble(elements.get(10).getText());
                    clearancePerGame = getDouble(elements.get(11).getText());
                    wasDribbledPerGame = getDouble(elements.get(12).getText());
                    blocksPerGame = getDouble(elements.get(13).getText());
                    ownGoal = getNumber(elements.get(14).getText());

                    p.setDefensive(tacklesPerGame,interceptionsPerGame,foulsPerGame,offsidesPerGame,clearancePerGame,wasDribbledPerGame,blocksPerGame,ownGoal);
                    stagePlayers = replacePlayer(stagePlayers,p);
                    System.out.println(p.toString());
                }
            }else if(tab.getText().equals("Offensive")){
                JavascriptExecutor executor = (JavascriptExecutor)driver;
                executor.executeScript("arguments[0].click();", tab);
                wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.cssSelector("#statistics-table-offensive #player-table-statistics-body tr"),5));

                List<WebElement> rows = body.findElements(By.cssSelector("#statistics-table-offensive #player-table-statistics-body tr"));
                for (WebElement e: rows){
                    List<WebElement> elements = e.findElements(By.cssSelector("td"));

                    // Get Player ID
                    WebElement el = elements.get(2).findElement(By.cssSelector(".player-link"));
                    String playerLink = el.getAttribute("href");
                    int playerId = getNumber(playerLink.substring(playerLink.indexOf("Players/")+8,playerLink.indexOf("/Show")));

                    p = findPlayer(stagePlayers,playerId);

                    keyPassesPerGame = getDouble(elements.get(10).getText());
                    dribblesWonPerGame = getDouble(elements.get(11).getText());
                    fouledPerGame = getDouble(elements.get(12).getText());
                    dispossessedPerGame = getDouble(elements.get(14).getText());
                    turnoverPerGame = getDouble(elements.get(15).getText());

                    p.setOffensive(keyPassesPerGame,dribblesWonPerGame,fouledPerGame,dispossessedPerGame,turnoverPerGame);
                    stagePlayers = replacePlayer(stagePlayers,p);
                    System.out.println(p.toString());
                }
            }else if(tab.getText().equals("Passing")){
                JavascriptExecutor executor = (JavascriptExecutor)driver;
                executor.executeScript("arguments[0].click();", tab);
                wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.cssSelector("#statistics-table-passing #player-table-statistics-body tr"),5));

                List<WebElement> rows = body.findElements(By.cssSelector("#statistics-table-passing #player-table-statistics-body tr"));
                for (WebElement e: rows){
                    List<WebElement> elements = e.findElements(By.cssSelector("td"));

                    // Get Player ID
                    WebElement el = elements.get(2).findElement(By.cssSelector(".player-link"));
                    String playerLink = el.getAttribute("href");
                    int playerId = getNumber(playerLink.substring(playerLink.indexOf("Players/")+8,playerLink.indexOf("/Show")));

                    p = findPlayer(stagePlayers,playerId);

                    totalPassesPerGame = getDouble(elements.get(9).getText());
                    accurateCrossesPerGame = getDouble(elements.get(11).getText());
                    accurateLongPassesPerGame = getDouble(elements.get(12).getText());
                    accurateThroughBallPerGame = getDouble(elements.get(13).getText());

                    p.setPassing(totalPassesPerGame,accurateCrossesPerGame,accurateCrossesPerGame,accurateThroughBallPerGame);
                    stagePlayers = replacePlayer(stagePlayers,p);
                    System.out.println(p.toString());
                }
            }else if(tab.getText().equals("Detailed")){
                JavascriptExecutor executor = (JavascriptExecutor)driver;
                executor.executeScript("arguments[0].click();", tab);
                wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.cssSelector("#statistics-table-detailed #player-table-statistics-body tr"),5));

                List<WebElement> rows = body.findElements(By.cssSelector("#statistics-table-detailed #player-table-statistics-body tr"));
                for (WebElement e: rows){

                    List<WebElement> elements = e.findElements(By.cssSelector("td"));

                    // Get Player ID
                    WebElement el = elements.get(2).findElement(By.cssSelector(".player-link"));
                    String playerLink = el.getAttribute("href");
                    int playerId = getNumber(playerLink.substring(playerLink.indexOf("Players/")+8,playerLink.indexOf("/Show")));

                    p = findPlayer(stagePlayers,playerId);
                    shotsOBox = getDouble(elements.get(8).getText());
                    shotsSixYardBox = getDouble(elements.get(9).getText());
                    shotsPenaltyArea = getDouble(elements.get(10).getText());

                    p.setDetailed(shotsOBox,shotsSixYardBox,shotsPenaltyArea);
                    stagePlayers = replacePlayer(stagePlayers,p);
                    System.out.println(p.toString());
                }
            }
        }

        return stagePlayers;
    }

    public static StagePlayer findPlayer(List<StagePlayer> players, int playerId){
        for (StagePlayer p: players){
            if(playerId == p.getPlayerId()){
                return p;
            }
        }

        return new StagePlayer(playerId);
    }

    public static ObservableList<StagePlayer> replacePlayer(ObservableList<StagePlayer> players, StagePlayer player){
        for (StagePlayer p: players){
            if(player.getPlayerId() == p.getPlayerId()){
                p = player;
                return players;
            }
        }

        return players;
    }

    public static ObservableList<StagePlayer> combineList(ObservableList<StagePlayer> playersA,ObservableList<StagePlayer> playersB){
        playersB.forEach(e -> {
            playersA.add(e);
        });

        return playersA;
    }

    public static void waitForPlayersToLoad(WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.cssSelector("#player-table-statistics-body tr"),5));
    }

    public static ObservableList<StagePlayer> extractPlayerInformation(BrowserEngine webkit, Team team, String link, int counter) throws InterruptedException, IOException {
        ObservableList<StagePlayer> stagePlayers = FXCollections.observableArrayList();
        int stageId = getNumber(link.substring(link.indexOf("=")+1));

        try{
            Page page = webkit.navigate(link);
            WebDriver driver = new ChromeDriver();

//            (new WebDriverWait(driver, 10)).until(new ExpectedCondition<Boolean>() {
//                public Boolean apply(WebDriver d) {
//                    System.out.println(d.findElements(By.cssSelector("#player-table-statistics-body tr")).size());
//                    return d.findElements(By.cssSelector("#player-table-statistics-body tr")).size() > 0;
//                }
//            });
            driver.get(link);
            System.out.println(driver.getTitle()+" "+driver.getCurrentUrl());
            waitForPlayersToLoad(driver);
            page.getDocument()
                    .queryAll("#player-table-statistics-body tr")
                    .forEach(e -> {
                        List<Element> elements = e.queryAll("td");

                        Element el = elements.get(2).query(".player-link").get();
                        boolean isAvailable = true;
                        if(e.hasClass("not-current-player")){
                            isAvailable = false;
                        }
                        //Player's Name
                        String name = el.getInnerHTML();
                        String playerLink = el.getAttribute("href").get();
                        System.out.println(playerLink.substring(playerLink.indexOf("Players/")+8,playerLink.indexOf("/Show")));

                        //Player ID
                        int playerId = getNumber(playerLink.substring(playerLink.indexOf("Players/"),playerLink.indexOf("/Show")-5));

                        //Player shirtNumber and position
                        List<Element> playerDetails = elements.get(2).queryAll(".player-meta-data");
                        int shirtNumber = getNumber(playerDetails.get(0).getInnerHTML());
                        String playerPositions = playerDetails.get(1).getInnerHTML();

                        //Player body details
                        int height = getNumber(elements.get(3).getInnerHTML());
                        int weight = getNumber(elements.get(4).getInnerHTML());

                        //Player Match stats
                        String appearance = elements.get(5).getInnerHTML();
                        int minutesPlayed = getNumber(elements.get(6).getInnerHTML());
                        int goals = getNumber(elements.get(7).getInnerHTML());
                        int assists = getNumber(elements.get(8).getInnerHTML());
                        int yellowCards = getNumber(elements.get(9).getInnerHTML());
                        int redCards = getNumber(elements.get(10).getInnerHTML());
                        String shotsPerGame = getDouble(elements.get(11).getInnerHTML());
                        String passSuccess = getDouble(elements.get(12).getInnerHTML());
                        String aerialsWon = getDouble(elements.get(13).getInnerHTML());
                        int manOfTheMatch = getNumber(elements.get(14).getInnerHTML());
                        String rating = getDouble(elements.get(15).getInnerHTML());

                        StagePlayer player = new StagePlayer(
                                stageId,
                                isAvailable,
                                name,
                                team.getName(),
                                team.getTeamId(),
                                playerId,
                                shirtNumber,
                                playerPositions,
                                height,
                                weight,
                                appearance,
                                minutesPlayed,
                                goals,
                                assists,
                                yellowCards,
                                redCards,
                                shotsPerGame,
                                passSuccess,
                                aerialsWon,
                                manOfTheMatch,
                                rating
                        );

                        stagePlayers.add(player);
                        System.out.println(player.toString());
                    });
        }catch (IndexOutOfBoundsException ex){
//            writeLog("ERROR :: Caught IndexOutOfBoundsException in Method 1 "+team.getName()+"  "+link);
            counter++;

            //If there are no stages found fall back wait for seven seconds then execute to method number 1 of extraction
            if(counter <= 15){
                System.out.println("Proceeding to trial Method 2");
                Thread.sleep(36000);
                return ExtractorJsoup.extractPlayers(webkit,team,link,counter);
//                return extractPlayerInformation(webkit,team,link,counter);
            }else{
                Extractor.writeLog("WARNING :: Could not gather team data for link "+team.getName());
                return stagePlayers;
            }
        }catch (Exception e){
            Thread.sleep(36000);

//            writeLog("ERROR :: Caught Unknown Exception in Method 1 "+team.getName()+"  "+link);
            counter++;

            //If there are no stages found fall back wait for seven seconds then execute to method number 1 of extraction
            if(counter <= 20){
                System.out.println("Going Back to Method 1");
                Thread.sleep(36000);
                return Extractor.extractPlayerInformation(webkit,team,link,counter);
            }else{
                Extractor.writeLog("WARNING :: Could not gather team data for the link "+link);
                return stagePlayers;
            }
        }

        if(stagePlayers.size() > 0){
            Extractor.writeLog("SUCCESS :: Method 1 Loading data was successful for the link "+link);
            return stagePlayers;
        }else{
            counter++;

            //If there are no stages found fall back wait for seven seconds then execute to method number 1 of extraction
            if(counter <= 20){
                System.out.println("Proceeding to trial Method 2");
                Thread.sleep(36000);
                return ExtractorJsoup.extractPlayers(webkit,team,link,counter);
//                return extractPlayerInformation(webkit,team,link,counter);
            }else{
                Extractor.writeLog("WARNING :: Could not gather team data for the link "+link);
                return stagePlayers;
            }
        }
    }

    public static int getNumber(String data){
        String number = "";
        if(data.equals("-")){
            return 0;
        }else{
            char [] _data = data.toCharArray();
            char [] integers = {'0','1','2','3','4','5','6','7','8','9'};

            for (int j = 0; j < _data.length; j++){
                char c = _data[j];
                for (int i = 0; i < integers.length; i++) {
                    if(integers[i] == c){
                        number += c;
                        break;
                    }
                }

                if(j == _data.length - 1 && number.length() > 0){
                    return Integer.parseInt(number);
                }
            }
        }

        return 0;
    }

    public static String getDouble(String data){
        String number = "";
        if(data.equals("-")){
            return "0";
        }else{
            return data;
        }
    }

    public static ObservableList<Team> getTeams(BrowserEngine webkit, String link) throws IOException, InterruptedException {
        counter++;
        ObservableList<Team> teams = FXCollections.observableArrayList();
        Page page = webkit.navigate(link);
        page.getDocument()
            .queryAll("tbody.standings a.team-link")
            .forEach(e -> {
                String url = "https://www.whoscored.com"+e.getAttribute("href").get();
                String name = e.getInnerHTML();
                int id = Integer.parseInt(url.substring(url.indexOf("/Team")+7,url.indexOf("/Show")));

                Team t = new Team(id,name,url);
                teams.add(t);
                System.out.println("\nDetails \n"+t.toString());
            });
        if(teams.size() > 1){
            counter = 0;
            return teams;
        }else{
            if(counter < 6){
                Thread.sleep(36000);
                return Extractor.getTeams(webkit,link);
            }else{
                counter = 0;
                return ExtractorJsoup.getTeams(link);
            }
        }
    }
}
