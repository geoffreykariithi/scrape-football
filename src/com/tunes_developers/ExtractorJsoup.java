package com.tunes_developers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tunes_developers.Models.Link;
import com.tunes_developers.Models.Stage;
import com.tunes_developers.Models.StagePlayer;
import com.tunes_developers.Models.Team;
import io.webfolder.ui4j.api.browser.BrowserEngine;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ExtractorJsoup {
    static String location = "G:\\Workspace\\web\\playground\\football\\links.json";
    static int counter = 0;

    public static ObservableList<Link> extractLinks(String base) throws IOException {
        Document doc = Jsoup.connect(base).get();
        Elements pages = doc.select("ul#popular-tournaments-list > li > a.pt");
        ObservableList<Link> links = FXCollections.observableArrayList();

        for (Element el : pages) {
            Document linkDoc = Jsoup.parse(el.outerHtml());
            String link = base+linkDoc.selectFirst("a").attr("href");
            String title = linkDoc.selectFirst("a").attr("title");
            String name = linkDoc.html().substring(0,linkDoc.html().indexOf("<span"));

            links.add(new Link(name,title,link));
        }

        //Save to file
        writeToFile(links);
        return links;
    }

    public static void writeToFile(ObservableList<Link> links) throws IOException {
        Gson gson = (new GsonBuilder()).setPrettyPrinting().create();
        String data = gson.toJson(links);
        File.writeText(location,data);
        System.out.println("Completed Writting Links\n Total Number Written: "+links.size());
    }

    public static ObservableList<Link> extractTeamInfo(ObservableList<Link> links) throws IOException, InterruptedException {
        for (Link link : links) {
            ObservableList<Team> teams = getTeams(link.link);
            link.setTeams(new ArrayList<>(teams.stream().collect(Collectors.toList())));

            //Save to file
            writeToFile(links);
        }

        return  links;
    }

    public static ObservableList<StagePlayer> extractPlayers(BrowserEngine webkit, Team team,String link, int counter) throws InterruptedException, IOException {
        //Sleep before Loading to download all contents
        int sleep = (int) Math.round(Math.random() * 10000);
        System.out.println("Waiting: " + sleep);
        Thread.sleep(sleep);

        int stageId = Extractor.getNumber(link.substring(link.indexOf("?")+1));
        ObservableList<StagePlayer> stagePlayers = FXCollections.observableArrayList();
        counter++;
        Document doc = Jsoup.connect(link).get();

        try{

            Elements stageElements = doc.select("#player-table-statistics-body tr");
            System.out.println(link+"   Elements Count: "+stageElements.size());

            for (Element e : stageElements) {
                Document linkDoc = Jsoup.parse(e.outerHtml());
                boolean isAvailable = true;
                if(e.hasClass("not-current-player")){
                    isAvailable = false;
                }

                List<Element> elements = e.select("td");

                if(elements.size() > 10){
                    Elements el = elements.get(2).select(".player-link");
                    String name = el.html();
                    String playerLink = el.attr("href");
                    System.out.println(playerLink.substring(playerLink.indexOf("Players/"),playerLink.indexOf("/Show")-5));

                    //Player ID
                    int playerId = Extractor.getNumber(playerLink.substring(playerLink.indexOf("Players/"),playerLink.indexOf("/Show")-5));

                    //Player shirtNumber and position
                    List<Element> playerDetails = elements.get(2).select(".player-meta-data");
                    int shirtNumber = Extractor.getNumber(playerDetails.get(0).html());
                    String playerPositions = playerDetails.get(1).html();

                    //Player body details
                    int height = Extractor.getNumber(elements.get(3).html());
                    int weight = Extractor.getNumber(elements.get(4).html());

                    //Player Match stats
                    String appearance = elements.get(5).html();
                    int minutesPlayed = Extractor.getNumber(elements.get(6).html());
                    int goals = Extractor.getNumber(elements.get(7).html());
                    int assists = Extractor.getNumber(elements.get(8).html());
                    int yellowCards = Extractor.getNumber(elements.get(9).html());
                    int redCards = Extractor.getNumber(elements.get(10).html());
                    String shotsPerGame = Extractor.getDouble(elements.get(11).html());
                    String passSuccess = Extractor.getDouble(elements.get(12).html());
                    String aerialsWon = Extractor.getDouble(elements.get(13).html());
                    int manOfTheMatch = Extractor.getNumber(elements.get(14).html());
                    String rating = Extractor.getDouble(elements.get(15).html());

                    StagePlayer player = new StagePlayer(
                            stageId,
                            isAvailable,
                            name,
                            team.getName(),
                            team.getTeamId(),
                            playerId,
                            shirtNumber,
                            playerPositions,
                            height,
                            weight,
                            appearance,
                            minutesPlayed,
                            goals,
                            assists,
                            yellowCards,
                            redCards,
                            shotsPerGame,
                            passSuccess,
                            aerialsWon,
                            manOfTheMatch,
                            rating
                    );

                    stagePlayers.add(player);
                    System.out.println(player.toString());
                }
            }
        }catch (IndexOutOfBoundsException ex){
//            Extractor.writeLog("ERROR:: Caught IndexOutOfBoundsException in Method 2 "+team.getName()+"  "+link);
            counter++;

            //If there are no stages found fall back wait for seven seconds then execute to method number 1 of extraction
            if(counter <= 15){
                System.out.println("Going Back to Method 1");
                Thread.sleep(7000);
                return Extractor.extractPlayerInformation(webkit,team,link,counter);
            }else{
                Extractor.writeLog("WARNING:: Could not gather team data for the link + "+link);
                return stagePlayers;
            }
        }catch (Exception e){
            Extractor.writeLog("ERROR:: Caught Unknown Exception in Method 2 "+team.getName()+"  "+link);
            Thread.sleep(30000);

//            System.out.println("Caught Unknown Exception in Method 2");
            counter++;

            //If there are no stages found fall back wait for seven seconds then execute to method number 1 of extraction
            if(counter <= 20){
                System.out.println("Going Back to Method 1");
                Thread.sleep(7000);
                return Extractor.extractPlayerInformation(webkit,team,link,counter);
            }else{
                Extractor.writeLog("WARNING:: Could not gather team data for the link "+link);
                return stagePlayers;
            }
        }

        if(stagePlayers.size() > 0){
            System.out.println("Method 2 Successfull");
            Extractor.writeLog("SUCCESS:: Method 2 Loading data was successful for the link "+link);
            return stagePlayers;
        }else {
            counter++;

            //If there are no stages found fall back wait for seven seconds then execute to method number 1 of extraction
            if(counter <= 15){
                System.out.println("Going Back to Method 1");
                Thread.sleep(7000);
                return Extractor.extractPlayerInformation(webkit,team,link,counter);
            }else{
                Extractor.writeLog("WARNING:: Could not gather team data for the link "+link);
                return stagePlayers;
            }
        }
    }

    public static ObservableList<Stage> extractStages(BrowserEngine webkit,String link, int counter) throws InterruptedException, IOException {
        int sleep = (int) Math.round(Math.random() * 10000);
        System.out.println("Waiting: " + sleep);
        Thread.sleep(sleep);
        ObservableList<Stage> stages = FXCollections.observableArrayList();

        counter++;
        Document doc = Jsoup.connect(link).get();
        Elements stageElements = doc.select("#stageId option");
        System.out.println(link+"   Elements Count: "+stageElements.size());

        for (Element el : stageElements) {
            Document linkDoc = Jsoup.parse(el.outerHtml());
            int id = Integer.parseInt(el.attr("value"));
            String name = el.html();

            Stage s = new Stage(id,name);
            stages.add(s);
        }

        if(stages.size() > 0){
            System.out.println("Method 2 Successfull");
            return stages;
        }else {
            counter++;

            //If there are no stages found fall back wait for seven seconds then execute to method number 1 of extraction
            if(counter <= 5){
                System.out.println("Going Back to Method 1");
                Thread.sleep(7000);
                return Extractor.extractStages(webkit,link,counter);
            }else{
                return stages;
            }
        }
    }

    public static ObservableList<Team> getTeams(String link) throws IOException, InterruptedException {
        counter++;
        Document doc = Jsoup.connect(link).get();
        Elements teamElements = doc.select("a.team-link");
        System.out.println(link+"Elements Count: "+teamElements.size());
        ObservableList<Team> teams = FXCollections.observableArrayList();

        for (Element el : teamElements) {
            Document linkDoc = Jsoup.parse(el.outerHtml());
            String url = link+linkDoc.selectFirst("a").attr("href");
            String name = linkDoc.selectFirst("a").html();
            int id = Integer.parseInt(url.substring(url.indexOf("/Team")+7,url.indexOf("/Show")));

            Team t = new Team(id,name,url);
            teams.add(t);
            System.out.println("\nDetails \n"+t.toString());
        }

        if(teams.size() > 1){
            counter = 0;
            return teams;
        }else {
            if(counter < 6){
                Thread.sleep(7000);
                return ExtractorJsoup.getTeams(link);
            }else{
                counter = 0;
                return teams;
            }
        }
    }
}
