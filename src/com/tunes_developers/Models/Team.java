package com.tunes_developers.Models;

import java.util.ArrayList;

public class Team {
    private int teamId;
    private String name;
    private String link;
    private ArrayList<Stage> stages;

    public Team(int teamId, String name, String link) {
        this.teamId = teamId;
        this.name = name;
        this.link = link;
    }

    public int getTeamId() {
        return teamId;
    }

    public String getName() {
        return name;
    }

    public String getLink() {
        return link;
    }

    public ArrayList<Stage> getStages() {
        return stages;
    }

    public void setStages(ArrayList<Stage> stages) {
        this.stages = stages;
    }

    @Override
    public String toString() {
        return "Team{" +
                "teamId=" + teamId +
                ", name='" + name + '\'' +
                ", link='" + link + '\'' +
            '}';
    }
}