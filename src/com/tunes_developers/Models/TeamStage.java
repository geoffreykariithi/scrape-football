package com.tunes_developers.Models;

import java.util.ArrayList;

public class TeamStage {
    private String country;
    private int teamId;
    private String team;
    private String link;
    private ArrayList<Stage> Stages = new ArrayList<Stage>();


    public TeamStage(String country, int teamId, String team, String link, ArrayList<Stage> stages) {
        this.country = country;
        this.teamId = teamId;
        this.team = team;
        this.link = link;
        Stages = stages;
    }
}
