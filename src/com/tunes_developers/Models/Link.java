package com.tunes_developers.Models;

import javafx.collections.ObservableList;

import java.util.ArrayList;

public class Link {
    public String name;
    public String country;
    public String link;
    public ArrayList<Team> teams;

    public Link(String name,String country, String link) {
        this.country = country;
        this.link = link;
    }

    public String getTitle() {
        return country;
    }

    public String getLink() {
        return link;
    }

    public String getName() {
        return name;
    }

    public ArrayList<Team> getTeams() {
        return teams;
    }

    public void setTeams(ArrayList<Team> teams) {
        this.teams = teams;
    }
}
