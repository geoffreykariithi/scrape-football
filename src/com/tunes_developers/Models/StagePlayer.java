package com.tunes_developers.Models;

public class StagePlayer {
    private int stageId = 0;
    private boolean isAvailable = false;
    private String name;
    private String team;
    private int teamId;
    private int playerId = 0;
    private int shirtNumber = 0;
    private String playerPosition;
    private int height = 0;
    private int weight = 0;
    private String appearance;
    private int minutesPlayed;
    private int goals = 0;
    private int assists = 0;
    private int yellowCards = 0;
    private int redCards = 0;
    private String shotsPerGame = "0";
    private String passSuccess = "0";
    private String aerialsWon = "0";
    private int manOfTheMatch = 0;
    private String rating = "0";
    private String tacklesPerGame = "0";
    private String interceptionsPerGame = "0";
    private String foulsPerGame = "0";
    private String offsidesPerGame = "0";
    private String clearancePerGame = "0";
    private String wasDribbledPerGame = "0";
    private String blocksPerGame = "0";
    private int ownGoal = 0;
    private String keyPassesPerGame = "0";
    private String dribblesWonPerGame = "0";
    private String fouledPerGame = "0";
    private String dispossessedPerGame = "0";
    private String turnoverPerGame = "0";
    private String totalPassesPerGame = "0";
    private String accurateCrossesPerGame = "0";
    private String accurateLongPassesPerGame = "0";
    private String accurateThroughBallPerGame = "0";
    private String shotsOBox = "0";
    private String shotsSixYardBox = "0";
    private String shotsPenaltyArea = "0";

    public StagePlayer(int playerId) {
        this.playerId = playerId;
    }

    public StagePlayer(int stageId, boolean isAvailable, String name, String team, int teamId, int playerId, int shirtNumber, String playerPosition, int height, int weight, String appearance, int minutesPlayed, int goals, int assists, int yellowCards, int redCards, String shotsPerGame, String passSuccess, String aerialsWon, int manOfTheMatch, String rating) {
        this.stageId = stageId;
        this.isAvailable = isAvailable;
        this.name = name;
        this.team = team;
        this.teamId = teamId;
        this.playerId = playerId;
        this.shirtNumber = shirtNumber;
        this.playerPosition = playerPosition;
        this.height = height;
        this.weight = weight;
        this.appearance = appearance;
        this.minutesPlayed = minutesPlayed;
        this.goals = goals;
        this.assists = assists;
        this.yellowCards = yellowCards;
        this.redCards = redCards;
        this.shotsPerGame = shotsPerGame;
        this.passSuccess = passSuccess;
        this.aerialsWon = aerialsWon;
        this.manOfTheMatch = manOfTheMatch;
        this.rating = rating;
    }

    public void setDefensive(
            String tacklesPerGame,
            String interceptionsPerGame,
            String foulsPerGame,
            String offsidesPerGame,
            String clearancePerGame,
            String wasDribbledPerGame,
            String blocksPerGame,
            int ownGoal){

        this.tacklesPerGame = tacklesPerGame;
        this.interceptionsPerGame = interceptionsPerGame;
        this.foulsPerGame = foulsPerGame;
        this.offsidesPerGame = offsidesPerGame;
        this.clearancePerGame = clearancePerGame;
        this.wasDribbledPerGame = wasDribbledPerGame;
        this.blocksPerGame = blocksPerGame;
        this.ownGoal = ownGoal;
    }

    public void setOffensive(
            String keyPassesPerGame,
            String dribblesWonPerGame,
            String fouledPerGame,
            String dispossessedPerGame,
            String turnoverPerGame){

        this.keyPassesPerGame = keyPassesPerGame;
        this.dribblesWonPerGame = dribblesWonPerGame;
        this.fouledPerGame = fouledPerGame;
        this.dispossessedPerGame = dispossessedPerGame;
        this.turnoverPerGame = turnoverPerGame;
    }

    public void setPassing(
            String totalPassesPerGame,
            String accurateCrossesPerGame,
            String accurateLongPassesPerGame,
            String accurateThroughBallPerGame){

        this.totalPassesPerGame = totalPassesPerGame;
        this.accurateCrossesPerGame = accurateCrossesPerGame;
        this.accurateLongPassesPerGame = accurateLongPassesPerGame;
        this.accurateThroughBallPerGame = accurateThroughBallPerGame;
    }

    public void setDetailed(
            String shotsOBox,
            String shotsSixYardBox,
            String shotsPenaltyArea){

        this.shotsOBox = shotsOBox;
        this.shotsSixYardBox = shotsSixYardBox;
        this.shotsPenaltyArea = shotsPenaltyArea;
    }

    public int getStageId() {
        return stageId;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public String getName() {
        return name;
    }

    public String getTeam() {
        return team;
    }

    public int getTeamId() {
        return teamId;
    }

    public int getPlayerId() {
        return playerId;
    }

    public int getShirtNumber() {
        return shirtNumber;
    }

    public String getPlayerPosition() {
        return playerPosition;
    }

    public int getHeight() {
        return height;
    }

    public int getWeight() {
        return weight;
    }

    public String getApperance() {
        return appearance;
    }

    public int getMinutesPlayed() {
        return minutesPlayed;
    }

    public int getGoals() {
        return goals;
    }

    public int getAssists() {
        return assists;
    }

    public int getYellowCards() {
        return yellowCards;
    }

    public int getRedCards() {
        return redCards;
    }

    public String getShotsPerGame() {
        return shotsPerGame;
    }

    public String getPassSuccess() {
        return passSuccess;
    }

    public String getAerialsWon() {
        return aerialsWon;
    }

    public int getManOfTheMatch() {
        return manOfTheMatch;
    }

    public String getRating() {
        return rating;
    }

    @Override
    public String toString() {
        return "StagePlayer{" +
                "stageId=" + stageId +
                ", isAvailable=" + isAvailable +
                ", name='" + name + '\'' +
                ", team='" + team + '\'' +
                ", teamId=" + teamId +
                ", playerId=" + playerId +
                ", shirtNumber=" + shirtNumber +
                ", playerPosition='" + playerPosition + '\'' +
                ", height=" + height +
                ", weight=" + weight +
                ", appearance='" + appearance + '\'' +
                ", minutesPlayed=" + minutesPlayed +
                ", goals=" + goals +
                ", assists=" + assists +
                ", yellowCards=" + yellowCards +
                ", redCards=" + redCards +
                ", shotsPerGame='" + shotsPerGame + '\'' +
                ", passSuccess='" + passSuccess + '\'' +
                ", aerialsWon='" + aerialsWon + '\'' +
                ", manOfTheMatch=" + manOfTheMatch +
                ", rating='" + rating + '\'' +
                ", tacklesPerGame='" + tacklesPerGame + '\'' +
                ", interceptionsPerGame='" + interceptionsPerGame + '\'' +
                ", foulsPerGame='" + foulsPerGame + '\'' +
                ", offsidesPerGame='" + offsidesPerGame + '\'' +
                ", clearancePerGame='" + clearancePerGame + '\'' +
                ", wasDribbledPerGame='" + wasDribbledPerGame + '\'' +
                ", blocksPerGame='" + blocksPerGame + '\'' +
                ", ownGoal=" + ownGoal +
                ", keyPassesPerGame='" + keyPassesPerGame + '\'' +
                ", dribblesWonPerGame='" + dribblesWonPerGame + '\'' +
                ", fouledPerGame='" + fouledPerGame + '\'' +
                ", dispossessedPerGame='" + dispossessedPerGame + '\'' +
                ", turnoverPerGame='" + turnoverPerGame + '\'' +
                ", totalPassesPerGame='" + totalPassesPerGame + '\'' +
                ", accurateCrossesPerGame='" + accurateCrossesPerGame + '\'' +
                ", accurateLongPassesPerGame='" + accurateLongPassesPerGame + '\'' +
                ", accurateThroughBallPerGame='" + accurateThroughBallPerGame + '\'' +
                ", shotsOBox='" + shotsOBox + '\'' +
                ", shotsSixYardBox='" + shotsSixYardBox + '\'' +
                ", shotsPenaltyArea='" + shotsPenaltyArea + '\'' +
                '}';
    }
}
